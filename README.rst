Flask-CSRF
##########

CSRF Protection for Flask applications, if WTForms is too much for a dependency.

This is a shameless extract of the CSRF protection part of `Flask-WTF <https://flask-wtf.readthedocs.io/>`_.

Installation
============

.. code-block:: sh

   pip install Flask-CSRF

Where is the documentation?
===========================

Here. This package is so small i didn’t deem it necessary to have a Read The Docs page.  All the
usage examples fit in a small section of this README file.

Usage
=====

.. code-block:: python

   from flask import Flask
   from flask_csrf import CSRFProtect

   app = Flask(__name__)
   csrf = CSRFProtect(app=app)    :param token_key: Key where token is stored in session for comparision.


You can also use it with application factories:

.. code-block:: python

   csrf = CSRFProtect()

   def create_app():
       app = Flask(__name__)
       csrf.init_app(app)

Then, you can use the ``csrf_field_name`` and ``csrf_token`` template variables to insert a CSRF
token to the rendered templates.

.. code-block:: jinja

   <input type="hidden" name="{{ csrf_field_name }}" value="{{ csrf_token }}">

The CSRF token is automatically validated for unsafe requests (POST, PUT, PATCH, and DELETE by
default).

.. note::

   The ``csrf_field_name`` and ``csrf_token`` template globals will only be available after you
   call ``CSRFProtect.init_app()``.

.. warning::

   If you are coming from Flask-WTF land, be aware that ``csrf_token`` is not a function in
   Flask-CSRF, but a proxy object that evaluates to the actual CSRF token.

Exempting views and blueprints
------------------------------

If you want specific views, or whole blueprints from CSRF protection for any reason, you can use
the ``CSRFProtect.exempt()`` method.

.. code-block:: python

   csrf = CSRFProtect(app)

   @app.route('/some-view', methods=['POST'])
   @csrf.exempt
   def some_view():
       ...

   bp = Blueprint(...)
   csrf.exempt(bp)

Catching CSRF errors
--------------------

If a CSRF token is not found or found invalid, a ``flask_csrf.CSRFError`` exception is thrown.
This exception is a subclass of werkzeug’s ``BadRequest``, so you will end up seeing a HTTP 400
Bad Request page.  If you use this error for other purposes and you want to do something different
for CSRF errors, you can catch this exception explicitly:

.. code-block:: python

   @app.errorhandler(flask_csrf.CSRFError)
   def handle_csrf_error(error):
       ...

Configuration
=============

============================  ======================================  =========================================================================================================================
Variable name                 Default                                 Purpose
----------------------------  --------------------------------------  -------------------------------------------------------------------------------------------------------------------------
``FLASK_CSRF_ENABLED``        ``True``                                Set to ``False`` to disable all CSRF protection.
``FLASK_CSRF_CHECK_DEFAULT``  ``True``                                When using the CSRF protection extension, this controls whether every view is protected by default.
``FLASK_CSRF_SECRET_KEY``     ``current_app.secret_key``              Random data for generating secure tokens.
``FLASK_CSRF_METHODS``        ``['POST', 'PUT', 'PATCH', 'DELETE']``  HTTP methods to protect from CSRF.
``FLASK_CSRF_FIELD_NAME``     ``"csrf_token"``                        Name of the form field and session key that holds the CSRF token.
``WTF_CSRF_HEADERS``          ``['X-CSRFToken', 'X-CSRF-Token']``     HTTP headers to search for CSRF token when it is not provided in the form.
``FLASK_CSRF_TIME_LIMIT``     ``3600``                                Max age in seconds for CSRF tokens. If set to ``None``, the CSRF token is valid for the life of the session.
``FLASK_CSRF_SSL_STRICT``     ``True``                                Whether to enforce the same origin policy by checking that the referrer matches the host. Only applies to HTTPS requests.
============================  ======================================  =========================================================================================================================


Contribute
==========

Check the `issues <https://gitlab.com/gergelypolonkai/flask-csrf/issues>`_ first!  It is possible
that someone has reported your problem or requested your feature.  If you can’t find anything
related, go on and open an issue and let’s discuss if it’s relevant for the project.  If it is,
nobody is working on it yet, and you want to, read on!

1. Fork the repository on `GitLab <https://gitlab.com/gergelypolonkai/flask-csrf>`_.
2. Fix the bug you found or implement the change you need.
3. Push it to your fork
4. Open a Merge Request

License
=======

Copyright (c) 2019 by Gergely Polonkai.

Some rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
  with the distribution.
- The names of the contributors may not be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
