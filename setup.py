#! /usr/bin/env python

from distutils.core import setup


setup(name='Flask-CSRF',
      version='0.1.0',
      description='',
      long_description='',
      author='Gergely Polonkai',
      author_email='gergely@polonkai.eu',
      url='https://gitlab.com/gergelypolonkai/flask-csrf',
      packages=['flask_csrf'],
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Web Environment',
          'Framework :: Flask',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Programming Language :: Python :: 3.6',
          'Topic :: Internet :: WWW/HTTP :: Session',
          'Topic :: Security',
      ],
      license='BSD',
      maintainer='Gergely Polonkai',
      maintainer_email='gergely@polonkai.eu',
      requires=[
          'Flask (>=1.0)',
          'itsdangerous (>=1.1)',
          'werkzeug (>=0.16.0)',
      ])
