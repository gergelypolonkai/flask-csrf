"""Configuration and global fixtures for PyTest
"""

from flask import Flask as _Flask, g, request, session
import pytest

from flask_csrf import CSRFProtect, generate_csrf


class Flask(_Flask):
    """Flask subclass to provide the necessary settings and handle None responses
    """

    testing = True
    secret_key = __name__

    def make_response(self, rv):
        if rv is None:
            rv = ''

        return super(Flask, self).make_response(rv)


@pytest.fixture
def app():
    """A Flask application to test with
    """

    app_ = Flask(__name__)

    CSRFProtect(app_)

    @app_.route('/', methods=['GET', 'POST'])
    def index():  # pylint: disable=unused-variable
        return ''

    @app_.after_request
    def add_csrf_header(response):  # pylint: disable=unused-variable
        response.headers.set('X-CSRF-Token', generate_csrf())

        if request.args.get('mangle'):
            delattr(g, 'csrf_token')
            del session['csrf_token']
            generate_csrf()

        return response

    return app_


@pytest.yield_fixture
def app_ctx(app):  # pylint: disable=redefined-outer-name
    """An application context
    """

    with app.app_context() as ctx:
        yield ctx


@pytest.yield_fixture
def req_ctx(app):  # pylint: disable=redefined-outer-name
    """A request context
    """

    with app.test_request_context() as ctx:
        yield ctx
