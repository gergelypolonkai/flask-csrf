"""Tests for the main flask_csrf module
"""

import logging

from flask import Blueprint, render_template_string
import pytest

from flask_csrf import __version__, _get_config, CSRFError, generate_csrf, logger


def test_version():
    """Check if the module version number is set correctly
    """

    assert __version__ == '0.1.0'


@pytest.fixture
def csrf(app):
    """Fixture providing easy access to the CSRF Flask extension
    """

    return app.extensions['csrf']


def test_render_token(req_ctx):  # pylint: disable=unused-argument
    """Test rendering the CSRF token
    """

    token = generate_csrf()

    assert render_template_string('{{ csrf_token }}') == token


def test_protect_missing(client):
    """Test if CSRF protection fails if the token is not sent with the form
    """

    response = client.post('/')
    assert response.status_code == 400
    assert 'The CSRF token is missing.' in response.get_data(as_text=True)


@pytest.mark.parametrize('prefixed', (True, False), ids=('plain', 'prefixed'))
def test_protect_empty(prefixed, client):
    """Test if CSRF protection fails if the token sent is empty
    """

    key = 'csrf_token'

    if prefixed:
        key = 'prefix-{}'.format(key)

    response = client.post('/', data={key: ''})
    assert response.status_code == 400
    assert 'The CSRF token is missing.' in response.get_data(as_text=True)


def test_protect_missing_from_session(client):
    """Test if CSRF protection fails if the token is not saved in the session
    """

    response = client.post('/', data={'csrf_token': 'something'})
    assert response.status_code == 400
    assert 'The CSRF session token is missing.' in response.get_data(as_text=True)


def test_protect_token_expired(app, client):
    """Test if CSRF protection fails if the token has expired
    """

    token = client.get('/').headers['X-CSRF-Token']

    app.config['FLASK_CSRF_TIME_LIMIT'] = -1
    response = client.post('/', data={'csrf_token': token})
    del app.config['FLASK_CSRF_TIME_LIMIT']
    assert response.status_code == 400
    assert 'The CSRF token has expired.' in response.get_data(as_text=True)


def test_protect_bad_data(client):
    """Test if CSRF protection fails if the token data sent is invalid
    """

    client.get('/')

    response = client.post('/', data={'csrf_token': 'baddata'})
    assert response.status_code == 400
    assert 'The CSRF token is invalid.' in response.get_data(as_text=True)


def test_protect_csrf_disabled(app, client):
    """Test if CSRF protection doesn’t fail if it’s actually disabled
    """

    app.config['FLASK_CSRF_ENABLED'] = False
    assert client.post('/').get_data() == b''
    app.config['FLASK_CSRF_ENABLED'] = True


def test_protect_no_check_default(app, client):
    """Test if CSRF protection doesn’t fail if it’s set not to check by default
    """

    app.config['FLASK_CSRF_CHECK_DEFAULT'] = False
    assert client.post('/').get_data() == b''
    app.config['FLASK_CSRF_CHECK_DEFAULT'] = True


@pytest.mark.parametrize('method', ('GET', 'OPTIONS', 'HEAD'))
def test_protect_safe_method(method, client, app_ctx):  # pylint: disable=unused-argument
    """Test if CSRF protection is not applied to safe methods
    """

    assert client.open('/', method=method).status_code == 200


def test_protect_404(client):
    """Test if posting to a non-existing endpoint results in 404

    …instead of :class:`flask_csrf.CSRFError`, even though the CSRF token is not sent.
    """

    assert client.post('/not-found').status_code == 404


@pytest.mark.parametrize('prefixed', (True, False), ids=('plain', 'prefixed'))
@pytest.mark.parametrize('method', ('POST', 'PUT', 'PATCH', 'DELETE'))
def test_protect_post(prefixed, method, client):
    """Test if CSRF protection is applied to unsafe methods
    """

    token = client.open('/', method=method).headers['X-CSRF-Token']

    key = 'csrf_token'

    if prefixed:
        key = 'prefix-{}'.format(key)

    assert client.post('/', data={key: token}).status_code == 200


def test_protect_header(client):
    """Test if CSRF protection works if the token is sent in the headers
    """

    token = client.get('/').headers['X-CSRF-Token']
    assert client.post('/', headers={'X-CSRF-Token': token}).status_code == 200


def test_protect_invalid_token(client):
    """Test if CSRF validation fails if the token is invalid
    """

    token = client.get('/?mangle=1').headers['X-CSRF-Token']
    response = client.post('/', data={'csrf_token': token})
    assert response.status_code == 400
    assert 'The CSRF tokens do not match.' in response.get_data(as_text=True)


def test_same_origin_missing_referer(client):
    """Test if same-origin checking fails if the Referer header is missing
    """

    token = client.get('/').headers['X-CSRF-Token']
    response = client.post('/', base_url='https://localhost', headers={
        'X-CSRF-Token': token
    })
    data = response.get_data(as_text=True)
    assert 'The referrer header is missing.' in data


def test_same_origin_referer_scheme_mismatch(client):
    """Test if same-origin checking fails of only the scheme is different
    """

    token = client.get('/').headers['X-CSRF-Token']
    response = client.post('/', base_url='https://localhost', headers={
        'X-CSRF-Token': token, 'Referer': 'http://localhost/'
    })
    data = response.get_data(as_text=True)
    assert 'The referrer does not match the host.' in data


def test_same_origin_referer_address_mismatch(client):
    """Test if same-origin checking fails for different hosts
    """

    token = client.get('/').headers['X-CSRF-Token']
    response = client.post('/', base_url='https://localhost', headers={
        'X-CSRF-Token': token, 'Referer': 'https://other/'
    })
    data = response.get_data(as_text=True)
    assert 'The referrer does not match the host.' in data


def test_same_origin_port_mismatch(client):
    """Test if same-origin checking fails if only the port is different
    """

    token = client.get('/').headers['X-CSRF-Token']
    response = client.post('/', base_url='https://localhost', headers={
        'X-CSRF-Token': token, 'Referer': 'https://localhost:8080/'
    })
    data = response.get_data(as_text=True)
    assert 'The referrer does not match the host.' in data


def test_same_origin_match(client):
    """Test if same-origin is taken into account
    """

    token = client.get('/').headers['X-CSRF-Token']
    response = client.post('/', base_url='https://localhost', headers={
        'X-CSRF-Token': token, 'Referer': 'https://localhost/'
    })
    assert response.status_code == 200


def test_exempt_view(app, csrf, client):  # pylint: disable=redefined-outer-name
    """Test if views can be exempt from CSRF protection
    """

    @app.route('/exempt', methods=['POST'])
    @csrf.exempt
    def _exempt():
        pass

    response = client.post('/exempt')
    assert response.status_code == 200

    csrf.exempt('tests.conftest.index')
    response = client.post('/')
    assert response.status_code == 200


def test_manual_protect(app, csrf, client):  # pylint: disable=redefined-outer-name
    """Test if views exempt from CSRF protection can be manually protected
    """

    @app.route('/manual', methods=['GET', 'POST'])
    @csrf.exempt
    def _manual():  # pylint: disable=unused-variable
        csrf.protect()

    response = client.get('/manual')
    assert response.status_code == 200

    response = client.post('/manual')
    assert response.status_code == 400


def test_exempt_blueprint(app, csrf, client):  # pylint: disable=redefined-outer-name
    """Test if whole blueprints can be exempt from CSRF protection
    """

    blueprint = Blueprint('exempt', __name__, url_prefix='/exempt')
    csrf.exempt(blueprint)

    @blueprint.route('/', methods=['POST'])
    def index():  # pylint: disable=unused-variable
        pass

    app.register_blueprint(blueprint)
    response = client.post('/exempt/')
    assert response.status_code == 200


def test_error_handler(app, client):
    """Test if error handlers are correctly invoked for CSRF errors
    """

    @app.errorhandler(CSRFError)
    def _handle_csrf_error(error):
        return error.description.lower()

    response = client.post('/')
    assert response.get_data(as_text=True) == 'the csrf token is missing.'


def test_validate_error_logged(client, caplog):
    """Test if validation errors gets logged
    """

    logger.setLevel(logging.INFO)

    client.post('/')
    assert caplog.record_tuples == [
        ('flask_csrf', logging.INFO, 'The CSRF token is missing.')
    ]


def test_get_config_missing(app):  # pylint: disable=unused-argument
    """Test getting a config value that doesn’t exist
    """

    with pytest.raises(RuntimeError):
        _get_config(None, 'TEST_CONFIG', required=True)
